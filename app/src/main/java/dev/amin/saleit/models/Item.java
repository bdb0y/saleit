package dev.amin.saleit.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "item")
public class Item {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private boolean nationality;
    private double price;

    public void setId(int id) {
        this.id = id;
    }

    public void setNationality(boolean nationality) {
        this.nationality = nationality;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isNationality() {
        return nationality;
    }

    public double getPrice() {
        return price;
    }


    public Item(String name, boolean nationality, double price) {
        this.name = name;
        this.nationality = nationality;
        this.price = price;
    }
}
