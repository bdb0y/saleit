package dev.amin.saleit.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "customer")
public class Customer {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String firstAndLastName;

    public Customer(String firstAndLastName) {
        this.firstAndLastName = firstAndLastName;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getFirstAndLastName() {
        return firstAndLastName;
    }
}
