package dev.amin.saleit.models;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "item_transaction", foreignKeys = {@ForeignKey(entity = Transaction.class,
        parentColumns = "id",
        childColumns = "transactionId"), @ForeignKey(entity = Item.class,
        parentColumns = "id",
        childColumns = "itemId")},
        indices = {
                @Index(name = "transaction_id", value = "transactionId"),
                @Index(name = "item_id", value = "itemId")
        })
public class ItemTransaction {
    @PrimaryKey
    private int id;
    private int transactionId;
    private int itemId;
    private int amount;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public int getItemId() {
        return itemId;
    }

    public int getAmount() {
        return amount;
    }

    public ItemTransaction(int transactionId, int itemId, int amount) {
        this.transactionId = transactionId;
        this.itemId = itemId;
        this.amount = amount;
    }

}