package dev.amin.saleit.models;

import java.util.Date;

public class Installment {
    private int id, customerId, transactionId, count;
    private String customerName;
    private double price;
    private Date date;

    public Installment(int customerId, int transactionId, double price, Date date) {
        this.customerId = customerId;
        this.transactionId = transactionId;
        this.price = price;
        this.date = date;
    }

    public Installment(String customerName, double price, int count) {
        this.customerName = customerName;
        this.price = price;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public String getCustomerName() {
        return customerName;
    }

    public int getId() {
        return id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public double getPrice() {
        return price;
    }

    public Date getDate() {
        return date;
    }

    public void setId(int id) {
        this.id = id;
    }
}
