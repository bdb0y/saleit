package dev.amin.saleit.models;

public class CartItem {
    private int amount;
    private String name;
    private double price;

    public CartItem(int amount, String name, double price) {
        this.amount = amount;
        this.name = name;
        this.price = price;
    }

    public CartItem(String name, double price) {
        this.amount = 0;
        this.name = name;
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void incrementAmount() {
        ++this.amount;
    }

    public void decrementAmount() {
        if (this.amount > 0) --this.amount;
    }
}
