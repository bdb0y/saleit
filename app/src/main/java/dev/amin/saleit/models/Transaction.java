package dev.amin.saleit.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "transaction_table")
public class Transaction {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String customerName;

    private int numberOfInstallments;

    private String inviterName;

    private int offPercentage;

    private String date;

    private double paid;
    private double price;
    private boolean done;

    public Transaction(String customerName, int numberOfInstallments, String inviterName,
                       int offPercentage, String date, double paid, double price,
                       boolean done) {
        this.customerName = customerName;
        this.numberOfInstallments = numberOfInstallments;
        this.inviterName = inviterName;
        this.offPercentage = offPercentage;
        this.date = date;
        this.paid = paid;
        this.price = price;
        this.done = done;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public int getNumberOfInstallments() {
        return numberOfInstallments;
    }

    public String getInviterName() {
        return inviterName;
    }

    public int getOffPercentage() {
        return offPercentage;
    }

    public String getDate() {
        return date;
    }

    public double getPaid() {
        return paid;
    }

    public double getPrice() {
        return price;
    }

    public boolean isDone() {
        return done;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", customerName='" + customerName + '\'' +
                ", numberOfInstallments=" + numberOfInstallments +
                ", inviterName='" + inviterName + '\'' +
                ", offPercentage=" + offPercentage +
                ", date=" + date +
                ", paid=" + paid +
                ", price=" + price +
                ", done=" + done +
                '}';
    }
}
