package dev.amin.saleit.view_models;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import dev.amin.saleit.models.ItemTransaction;
import dev.amin.saleit.repositories.ItemTransactionRepository;

public class ItemTransactionViewModel extends AndroidViewModel {

    private ItemTransactionRepository itemTransactionRepository;
    private LiveData<List<ItemTransaction>> itemTransactions;

    public ItemTransactionViewModel(@NonNull Application application) {
        super(application);
        itemTransactionRepository = new ItemTransactionRepository(application);
        itemTransactions = itemTransactionRepository.getCustomers();
    }

    public void insertItemTransaction(ItemTransaction itemTransaction) {
        itemTransactionRepository.insert(itemTransaction);
    }

    public void updateItemTransaction(ItemTransaction itemTransaction) {
        itemTransactionRepository.update(itemTransaction);
    }

    public void deleteItemTransaction(ItemTransaction itemTransaction) {
        itemTransactionRepository.delete(itemTransaction);
    }

    public LiveData<List<ItemTransaction>> getItemTransactions() {
        return itemTransactions;
    }
}