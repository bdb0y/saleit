package dev.amin.saleit.view_models;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import dev.amin.saleit.models.Item;
import dev.amin.saleit.repositories.ItemRepository;

public class ItemViewModel extends AndroidViewModel {

    private ItemRepository repository;
    private LiveData<List<Item>> items;

    public ItemViewModel(@NonNull Application application) {
        super(application);
        repository = new ItemRepository(application);
        items = repository.getItems();
    }

    public void insertItem(Item item){
        repository.insertItem(item);
    }

    public void updateItem(Item item){
        repository.updateItem(item);
    }

    public void deleteItem(Item item){
        repository.deleteItem(item);
    }

    public LiveData<List<Item>> getItems(){
        return items;
    }
}
