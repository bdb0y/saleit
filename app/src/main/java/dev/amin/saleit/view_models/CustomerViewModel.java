package dev.amin.saleit.view_models;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import dev.amin.saleit.models.Customer;
import dev.amin.saleit.repositories.CustomerRepository;

public class CustomerViewModel extends AndroidViewModel {

    private CustomerRepository customerRepository;
    private LiveData<List<Customer>> customers;

    public CustomerViewModel(@NonNull Application application) {
        super(application);
        customerRepository = new CustomerRepository(application);
        customers = customerRepository.getCustomers();
    }

    public void insertCustomer(Customer customer){
        customerRepository.insert(customer);
    }

    public void updateCustomer(Customer customer){
        customerRepository.update(customer);
    }

    public void daleteCustomer(Customer customer){
        customerRepository.delete(customer);
    }

    public LiveData<List<Customer>> getCustomers(){
        return customers;
    }
}
