package dev.amin.saleit.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import dev.amin.saleit.R;
import dev.amin.saleit.adapters.recycler_view_adapters.RecyclerViewAdapter;
import dev.amin.saleit.models.Transaction;
import dev.amin.saleit.view_models.TransactionViewModel;

public class SaleFragment extends Fragment {

    private List<Transaction> transactions = new ArrayList<>();
    private RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(transactions);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sale, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = view.findViewById(R.id.theSaleRecyclerView);


        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.addItemDecoration(
                new dev.amin.saleit.customization.DividerItemDecoration(recyclerView.getContext(), R.drawable.divider));


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                FloatingActionButton floatingActionButton
//                        = Objects.requireNonNull
//                        (getActivity()).findViewById(R.id.mainFloatingActionButton);
//                if (dy > 0)
//                    floatingActionButton.hide();
//                else floatingActionButton.show();
            }
        });

        TransactionViewModel transactionViewModel = ViewModelProviders.of(this)
                .get(TransactionViewModel.class);

        transactionViewModel.getTransactions().observe(this, transactions -> recyclerViewAdapter.setTransactions(transactions));
    }
}

//class Operation extends AsyncTask<Object, Void, Void> {
//
//    @Override
//    protected Void doInBackground(Object... objects) {
//        List<Transaction> transactions = (List<Transaction>) objects[0];
//        RecyclerViewAdapter adapter = (RecyclerViewAdapter) objects[1];
//        for (int i = 0; i < 20; i++) {
//            transactions.add(new Transaction(420_000));
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            adapter.notifyItemInserted(0);
//        }
//        return null;
//    }
//}