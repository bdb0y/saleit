package dev.amin.saleit.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import dev.amin.saleit.R;
import dev.amin.saleit.adapters.recycler_view_adapters.InstallmentRecyclerView;
import dev.amin.saleit.customization.DividerItemDecoration;
import dev.amin.saleit.models.Installment;

public class InstallmentFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_installment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<Installment> installments = new ArrayList<>();

        RecyclerView recyclerView = view.findViewById(R.id.theInstallmentRecyclerView);
        InstallmentRecyclerView adapter =
                new InstallmentRecyclerView(installments);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(
                        new DividerItemDecoration(recyclerView.getContext(), R.drawable.divider));

        installments.add(new Installment("رضائی زاده",420_000,2));
        installments.add(new Installment("رضائی زاده",420_000,1));
        installments.add(new Installment("رضائی زاده",420_000,2));
        installments.add(new Installment("رضائی زاده",420_000,1));
        installments.add(new Installment("رضائی زاده",420_000,2));
        installments.add(new Installment("رضائی زاده",420_000,1));
        installments.add(new Installment("رضائی زاده",420_000,2));
        installments.add(new Installment("رضائی زاده",420_000,1));
        installments.add(new Installment("رضائی زاده",420_000,2));
        installments.add(new Installment("رضائی زاده",420_000,2));
        installments.add(new Installment("رضائی زاده",420_000,1));
        installments.add(new Installment("رضائی زاده",420_000,1));
        adapter.notifyDataSetChanged();
    }
}
