package dev.amin.saleit.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import butterknife.ButterKnife;
import dev.amin.saleit.R;
import dev.amin.saleit.adapters.ViewPageAdapter;
import dev.amin.saleit.fragments.AnalyzeFragment;
import dev.amin.saleit.fragments.InstallmentFragment;
import dev.amin.saleit.fragments.SaleFragment;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawerLayout;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupToolbar();

        initializeItems();

        setupTabLayoutAndViewPager();

        setupToggle();

        setupNavigationViewItemListener();

        fabOnClickListener();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.theToolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        //Check to see which item was being clicked and perform appropriate action
        switch (menuItem.getItemId()) {
            //Replacing the main content with ContentFragment Which is our Inbox View;
            case R.id.theItems:
                startActivity(new Intent(this, ItemsActivity.class));
                drawerLayout.closeDrawer(Gravity.RIGHT);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null && item.getItemId() == android.R.id.home) {
            if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            } else {
                drawerLayout.openDrawer(Gravity.RIGHT);
            }
        }
        return false;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (toggle != null)
            toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (toggle != null)
            toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT))
            drawerLayout.closeDrawer(Gravity.RIGHT);
        else super.onBackPressed();
    }

    private void initializeItems() {
        drawerLayout = findViewById(R.id.theDrawerLayout);
        navigationView = findViewById(R.id.theNavigationView);
        floatingActionButton = findViewById(R.id.mainFloatingActionButton);
    }

    private void setupToggle() {
        toggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.openDrawer, R.string.closeDrawer);
        toggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(toggle);
    }

    private void fabOnClickListener() {
        floatingActionButton.setOnClickListener((v) ->
                startActivity(new Intent(this, AddTransactionActivity.class)));
    }

    private void setupTabLayoutAndViewPager() {
        ViewPager viewPager = findViewById(R.id.theHomeViewPage);
        TabLayout tabLayout = findViewById(R.id.theTabLayout);
        ViewPageAdapter viewPageAdapter =
                new ViewPageAdapter(getSupportFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPageAdapter.addFragment(new AnalyzeFragment(), "فاکتور");
        viewPageAdapter.addFragment(new InstallmentFragment(), "اقساط");
        viewPageAdapter.addFragment(new SaleFragment(), "فروش");
        viewPager.setAdapter(viewPageAdapter);
        viewPager.setCurrentItem(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 2:
                        floatingActionButton.setImageResource(R.drawable.ic_add_black_24dp);
                        break;
                    case 1:
                        floatingActionButton.setImageResource(R.drawable.ic_arrow_back_black_24dp);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void setupNavigationViewItemListener() {
        navigationView.setNavigationItemSelectedListener(this);
    }
}