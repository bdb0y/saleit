package dev.amin.saleit.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import dev.amin.saleit.R;
import dev.amin.saleit.Utilities.RecyclerViewOnItemClickListener;
import dev.amin.saleit.adapters.recycler_view_adapters.CartRecyclerView;
import dev.amin.saleit.models.CartItem;
import dev.amin.saleit.models.Item;
import dev.amin.saleit.models.Transaction;
import dev.amin.saleit.view_models.ItemViewModel;
import dev.amin.saleit.view_models.TransactionViewModel;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class AddTransactionActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CartRecyclerView adapter;
    private List<CartItem> cartItems;
    private AutoCompleteTextView customerName;
    private TextView finalPrice;
    private BottomSheetBehavior bottomSheetBehavior;
    private RelativeLayout priceSection;
    private ImageButton submitTransaction;
    private TextInputEditText transactionDate;
    private String chosenDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        setupToolbar();


        setupNumberOfInstallmentsPicker();

        setupInstallmentSection();


        initializeThings();

        recyclerView.setAdapter(adapter);


        ItemViewModel itemViewModel = ViewModelProviders.of(this)
                .get(ItemViewModel.class);

        itemViewModel.getItems().observe(this, items -> {
            List<CartItem> cartItemList = new ArrayList<>();
            for (Item item : items)
                cartItemList.add(new CartItem(item.getName(), item.getPrice()));
            cartItems = cartItemList;
            adapter.setItems(cartItems);
        });


        setupPriceSection();


        adapter.setOnThisItemClickListener(new RecyclerViewOnItemClickListener() {
            @Override
            public void OnItemClick(int position) {

            }

            @Override
            public void OnIncrement(int position) {
                CartItem cartItem = cartItems.get(position);
                String theFinal = cleanText(finalPrice.getText().toString());
                System.out.println(theFinal);
                DecimalFormat df = new DecimalFormat("#");
                double itemPrice = cartItem.getPrice();
                double theFinalPrice =
                        Double.parseDouble(theFinal);
//                System.out.println(df.format(itemPrice)+ " - " + df.format(theFinalPrice) + " - " +
//                        df.format(itemPrice + theFinalPrice));
                theFinalPrice += itemPrice;
                finalPrice.setText(format(df.format(theFinalPrice)));
                cartItem.incrementAmount();
                adapter.notifyItemChanged(position);
            }

            @Override
            public void OnDecrement(int position) {
                CartItem cartItem = cartItems.get(position);
                String finalString = cleanText(finalPrice.getText().toString());

                DecimalFormat df = new DecimalFormat("#");

                if (Double.parseDouble(finalString) > 0 && cartItem.getAmount() > 0) {
                    double price = cartItem.getPrice(), result =
                            Double.parseDouble(finalString);
                    result -= price;

                    finalPrice.setText(format(df.format(result)));
                }
                cartItem.decrementAmount();
                adapter.notifyItemChanged(position);
            }
        });


        String[] names = {"محمدامین رضائی زاده", "علی رضائی زاده", "الیاس متقی",
                "سیبشام صیلبشحجو زاده",
                "رزطرسام صلیبشحجو",
                "سام صلیبشیحجو",
                "قثصقسام رزظصلحجو",
                "فقفسام شذصلحجو",
                "ذزسام رزرزصلحجو",
                "ذزسام رزرزصلحجو",
                "ذزسام رزرزصلحجو",
                "ذزسام رزرزصلحجو",
                "ذزسام رزرزصلحجو",
                "ذزسام رزرزصلحجو",
                "ذزسام رزرزصلحجو",
                "مممسام زررزصلحجو",};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_dropdown_item_1line, names);

        customerName.setThreshold(1);
        customerName.setAdapter(arrayAdapter);

        editTextWatchers();


    }

    private void setupPriceSection() {
        priceSection.setOnClickListener(v -> {
            if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            else bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });
    }

    private void setupNumberOfInstallmentsPicker() {
        NumberPicker numberPicker = findViewById(R.id.addNumberOfInstallments);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(10);
    }

    private void setupInstallmentSection() {
        CheckBox checkBox = findViewById(R.id.addInstallmentCheckBox);
        LinearLayout installmentSection = findViewById(R.id.addInstallmentSection);
        checkBox.setOnCheckedChangeListener((view, isChecked) -> {
            if (isChecked)
                installmentSection.setVisibility(View.VISIBLE);
            else installmentSection.setVisibility(View.GONE);
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.addActivityToolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void editTextWatchers() {

        TextInputEditText prepaidEditText = findViewById(R.id.addPrePaidAmount);
        prepaidEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String theString = s.toString();
                if (!theString.equals(current) && !theString.isEmpty()) {
                    theString = formatText(theString);
                    current = theString;
                    prepaidEditText.setText(theString);
                    prepaidEditText.setSelection(theString.length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        TextInputEditText priceEditText = findViewById(R.id.addPrice);
        priceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String theString = s.toString();
                if (!theString.equals(current) && !theString.isEmpty()) {
                    theString = formatText(theString);
                    current = theString;
                    priceEditText.setText(theString);
                    priceEditText.setSelection(theString.length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        TextInputEditText inviterName = findViewById(R.id.addInviter),
                offPercentage = findViewById(R.id.addOffPercentage);
        NumberPicker numebrOfInstallments = findViewById(R.id.addNumberOfInstallments);

        transactionDate = findViewById(R.id.addDateOfTransaction);
        TextInputLayout date = findViewById(R.id.addDateOfTransactionContainer);

        PersianDate pdate = new PersianDate();
        PersianDateFormat pdformater1 = new PersianDateFormat("y F j");
        //1396/05/20
        transactionDate.setText(pdformater1.format(pdate));

        date.setOnClickListener(v -> showAlertDialog());

        submitTransaction.setOnClickListener(v -> {
            String name = customerName.getText().toString().trim().trim();
            String price = priceEditText.getText().toString().trim()
                    .replaceAll("[,٫.]", "");
            String inviter = inviterName.getText().toString();
            int numberOfInstallment = numebrOfInstallments.getValue();
            int offPer = Integer.parseInt(offPercentage.getText().toString());
            double pre = Double.parseDouble(cleanText(prepaidEditText.getText().toString()));
            TransactionViewModel transactionViewModel = ViewModelProviders.of(this)
                    .get(TransactionViewModel.class);
            transactionViewModel.insertTransaction(
                    new Transaction(name, numberOfInstallment, inviter, offPer, chosenDate, pre,
                            Double.parseDouble(price), false));
//            System.out.println(transactionViewModel.insertTransaction(new Tra));
        });
    }

    private void showAlertDialog() {
        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.date_picker_alert_dialog, null);
        builder.setView(customLayout);
        NumberPicker day = customLayout.findViewById(R.id.datePickerDay);
        NumberPicker month = customLayout.findViewById(R.id.datePickerMonth);
        NumberPicker year = customLayout.findViewById(R.id.datePickerYear);

        PersianDate pdate = new PersianDate();

        System.out.println(pdate.toDate().toString());

        PersianDateFormat pdformater1 = new PersianDateFormat("y/m/d");
        transactionDate.setText(pdformater1.format(pdate));
        day.setMinValue(1);
        day.setMaxValue(pdate.getMonthLength());
        day.setValue(pdate.getShDay());
        month.setMinValue(1);
        month.setMaxValue(12);
        month.setValue(pdate.getShMonth());
        month.setDisplayedValues(new String[]{"فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"});
        year.setMinValue(1380);
        year.setMaxValue(1500);
        year.setValue(pdate.getShYear());

        TextView dateValue = customLayout.findViewById(R.id.datePickerValue);
        String value = day.getValue() + " " +
                month.getDisplayedValues()[month.getValue() - 1] + " " + year.getValue();
        dateValue.setText(value);


        month.setOnValueChangedListener((numberPicker, preValue, currentValue) -> {
            day.setMaxValue(pdate.getMonthDays(year.getValue(), currentValue));
            String value1 = day.getValue() + " " +
                    month.getDisplayedValues()[month.getValue() - 1] + " " + year.getValue();
            dateValue.setText(value1);
        });


        year.setOnValueChangedListener(((numberPicker, preValue, currentValue) -> {
            day.setMaxValue(pdate.getMonthDays(currentValue, month.getValue()));
            String value2 = day.getValue() + " " +
                    month.getDisplayedValues()[month.getValue() - 1] + " " + year.getValue();
            dateValue.setText(value2);
        }));

        day.setOnValueChangedListener(((numberPicker, i, i1) -> {
            PersianDate persian = new PersianDate();
            persian.setShDay(i1);
            persian.setShMonth(month.getValue());
            persian.setShYear(year.getValue());
            String value3 = persian.dayName() + " " + i1 + " " +
                    month.getDisplayedValues()[month.getValue() - 1] + " " + year.getValue();
            dateValue.setText(value3);
        }));

        // add a button
        builder.setPositiveButton("OK", (dialog, which) -> {
            PersianDate persianDate = new PersianDate();
            persianDate.initJalaliDate(year.getValue(), month.getValue(), day.getValue());
            chosenDate = pdformater1.format(persianDate);
            String value3 = year.getValue() + "/" +
                    month.getValue() + "/" + day.getValue();
            String value4 = day.getValue() + " " +
                    month.getDisplayedValues()[month.getValue() - 1] + " " + year.getValue();
            System.out.println(value3);
            TextView addDateValue = findViewById(R.id.addDateValue);
            addDateValue.setText(dateValue.getText().toString());
            transactionDate.setText(value3);
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void initializeThings() {
        recyclerView = findViewById(R.id.addCartRecyclerView);
        cartItems = new ArrayList<>();
        adapter = new CartRecyclerView(cartItems);
        customerName = findViewById(R.id.addName);
        finalPrice = findViewById(R.id.theFinalAmount);
        LinearLayout bottomSheet = findViewById(R.id.addTransactionBottomSheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        priceSection = findViewById(R.id.addTransactionPriceSection);
        submitTransaction = findViewById(R.id.addSubmitButton);
    }

    @Override
    public boolean onSupportNavigateUp() {
        int state = bottomSheetBehavior.getState();
        if (state == BottomSheetBehavior.STATE_EXPANDED)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        else if (state == BottomSheetBehavior.STATE_SETTLING)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        else onBackPressed();

        return true;
    }

    @Override
    public void onBackPressed() {
        int state = bottomSheetBehavior.getState();
        if (state == BottomSheetBehavior.STATE_EXPANDED)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        else if (state == BottomSheetBehavior.STATE_SETTLING)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        else super.onBackPressed();
    }

    public String cleanText(String text) {
        return text.replaceAll("[,٫.]", "");
    }

    public String formatText(String text) {
        return NumberFormat.getInstance(Locale.US).format(
                Double.parseDouble(text.replaceAll("[,٫.]", ""))
        );
    }

    public String format(String text) {
        List<Character> unformatted = new ArrayList<>();
        int counter = 0;
        for (int i = text.length() - 1; i >= 0; i--) {
            ++counter;
            unformatted.add(text.charAt(i));
            if (counter == 3 && i != 0) {
                unformatted.add(',');
                counter = 0;
            }
        }
        System.out.println(unformatted);
        String result = "";
        for (int i = unformatted.size() - 1; i >= 0; i--) {
            result += unformatted.get(i);
        }
        return result;
    }
}
