package dev.amin.saleit.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputEditText;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import dev.amin.saleit.R;
import dev.amin.saleit.models.Item;
import dev.amin.saleit.view_models.ItemViewModel;

public class AddItemsActivity extends AppCompatActivity {

    private NumberPicker numberPicker;
    private ItemViewModel itemViewModel;
    private ImageButton submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_items);

        Toolbar toolbar = findViewById(R.id.activityAddItemsToolbar);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initializeThings();
        itemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);


        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(1);
        numberPicker.setDisplayedValues(new String[]{"خارجی", "ایرانی"});

        TextInputEditText itemPrice = findViewById(R.id.addItemsItemPrice);
        itemPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
                if (!text.equals(current) && !text.isEmpty()) {

                    String cleanString = text.replaceAll("[,٫.]", "");

                    String formatted = formatText(cleanString);
                    System.out.println(formatted);
                    current = formatted;
                    itemPrice.setText("");
                    itemPrice.setText(formatted);
                    itemPrice.setSelection(formatted.length());
                }

            }
        });
        TextInputEditText itemName = findViewById(R.id.addItemsItemName);

        submitButton.setOnClickListener(v -> {
            String name = itemName.getText().toString().trim(),
                    price = itemPrice.getText().toString();
            int nationality = numberPicker.getValue();

            if (!name.isEmpty() && !price.isEmpty()) {
                if (nationality == 0) {
                    itemViewModel.insertItem(new Item(name, true, Double.parseDouble(
                            price.replaceAll("[,٫.]", ""))));
                } else {
                    itemViewModel.insertItem(new Item(name, false, Double.parseDouble(
                            price.replaceAll("[,٫.]", ""))));
                }
                showCustomToast("کالا با موفقیت به لیست اضافه شد", Toast.LENGTH_SHORT);
                itemName.setText("");
                itemPrice.setText("");
                itemName.requestFocus();
            } else {
                showCustomToast("تمام فیلد ها اجباری هستند", Toast.LENGTH_LONG);
                if (name.isEmpty())
                    itemName.requestFocus();
                else itemPrice.requestFocus();
            }

        });

    }

    private void initializeThings() {
        numberPicker = findViewById(R.id.addItemsNationalityPicker);
        submitButton = findViewById(R.id.addItemsSubmitButton);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showCustomToast(String message, int length) {
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.custom_toast,
                findViewById(R.id.toastViewHolder));
        TextView textView = view.findViewById(R.id.customToastMessage);
        textView.setText(message.trim());

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(length);
        toast.setView(view);
        toast.show();
    }

    public String cleanText(String text) {
        return text.replaceAll("[,٫.]", "");
    }

    public String formatText(String text) {
        return NumberFormat.getInstance(Locale.US).format(
                Double.parseDouble(text.replaceAll("[,٫.]", ""))
        );
    }

    public String format(String text) {
        List<Character> unformatted = new ArrayList<>();
        int counter = 0;
        for (int i = text.length() - 1; i >= 0; i--) {
            ++counter;
            unformatted.add(text.charAt(i));
            if (counter == 3 && i != 0) {
                unformatted.add(',');
                counter = 0;
            }
        }
        System.out.println(unformatted);
        String result = "";
        for (int i = unformatted.size() - 1; i >= 0; i--) {
            result += unformatted.get(i);
        }
        return result;
    }
}