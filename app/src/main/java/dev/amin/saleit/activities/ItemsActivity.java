package dev.amin.saleit.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.amin.saleit.R;
import dev.amin.saleit.adapters.recycler_view_adapters.ItemsRecyclerView;
import dev.amin.saleit.customization.DividerItemDecoration;
import dev.amin.saleit.models.Item;
import dev.amin.saleit.view_models.ItemViewModel;

public class ItemsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ItemsRecyclerView adapter;
    private List<Item> items;
    private FloatingActionButton floatingActionButton;
    private RadioGroup radioGroup;
    private int filter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        Toolbar toolbar = findViewById(R.id.activityItemsToolbar);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initializeThings();

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(recyclerView.getContext(), R.drawable.divider));
        ItemViewModel itemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);
//        itemViewModel.getItems().observe(this,items -> {
//            adapter.setItems(items);
//        });


        itemViewModel.getItems().observe(this, items -> {
            adapter.setItems((items));
            this.items = items;
            filter = 0;
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    floatingActionButton.hide();
                } else floatingActionButton.show();
            }
        });

        floatingActionButton.setOnClickListener(v ->
                startActivity(new Intent(this, AddItemsActivity.class)));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.filterList) {
            Toast.makeText(this, "filter list is clicked", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeThings() {
        recyclerView = findViewById(R.id.theItemsRecyclerView);
        items = new ArrayList<>();
        adapter = new ItemsRecyclerView();
        floatingActionButton = findViewById(R.id.itemsFloatingActionButton);
        radioGroup = findViewById(R.id.filterGroup);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void openPopUp(MenuItem item) {
        Point p = new Point();
        p.x = 10;
        p.y = 10;
        showPopup(this, p);
    }

    private void showPopup(ItemsActivity context, Point p) {
        CardView viewGroup = context.findViewById(R.id.filterPopUpContainer);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = new View(this);
        if(layoutInflater != null)
            layout = layoutInflater.inflate(R.layout.filter_pop_up_menu, viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        popup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popup.setAnimationStyle(R.style.PopUpMenuAnimation);
        popup.setFocusable(true);

        radioGroup = layout.findViewById(R.id.filterGroup);
        RadioButton radioButton;
        switch (filter){
            case 0:
                radioButton = layout.findViewById(R.id.radioAll);
                radioButton.setChecked(true);
                break;
            case 1:
                radioButton = layout.findViewById(R.id.radioIran);
                radioButton.setChecked(true);
                break;
            case 2:
                radioButton = layout.findViewById(R.id.radioForeign);
                radioButton.setChecked(true);
                break;
        }
        radioGroup.setOnCheckedChangeListener((view, position) ->{
            switch (position){
                case R.id.radioAll:
                    adapter.setItems(items);
                    filter = 0;
                    break;
                case R.id.radioIran:
                    List<Item> iran = new ArrayList<>();
                    for(Item item : items)
                        if(!item.isNationality())
                            iran.add(item);
                    adapter.setItems(iran);
                    filter = 1;
                    break;
                case R.id.radioForeign:
                    List<Item> foreign = new ArrayList<>();
                    for(Item item : items)
                        if(item.isNationality())
                            foreign.add(item);
                    adapter.setItems(foreign);
                    filter = 2;
                    break;
            }
            recyclerView.scrollToPosition(0);
        });

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable(this.getResources()));

        // Displaying the popup at the specified location.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x, p.y);
        // Getting a reference to Close button, and close the popup when clicked.
//        Button close = (Button) layout.findViewById(R.drawable);
//        close.setOnClickListener(v -> popup.dismiss());
    }

}
