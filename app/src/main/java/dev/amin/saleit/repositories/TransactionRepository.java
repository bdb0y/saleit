package dev.amin.saleit.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import dev.amin.saleit.database.Database;
import dev.amin.saleit.database.data_access_objects.TransactionDao;
import dev.amin.saleit.models.Transaction;

public class TransactionRepository {
    private TransactionDao transactionDao;
    private LiveData<List<Transaction>> transactions;

    public TransactionRepository(Application application) {
        Database database = Database.getInstance(application);
        transactionDao = database.getTransactionDao();
        transactions = transactionDao.getTransactions();
    }

    public void insertTransaction(Transaction transaction) {
        new InsertTransactionTask(transactionDao).execute(transaction);
    }

    public void deleteTransaction(Transaction transaction) {
        new DeleteTransactionTask(transactionDao).execute(transaction);
    }

    public void updateTransaction(Transaction transaction) {
        new UpdateTransactionTask(transactionDao).execute(transaction);
    }

    public void deleteAllTransactions(){
        new DeleteAllTask(transactionDao).execute();
    }

    public LiveData<List<Transaction>> getTransactions() {
        return transactions;
    }

    private static class InsertTransactionTask extends AsyncTask<Transaction, Void, Void> {
        private TransactionDao transactionDao;

        private InsertTransactionTask(TransactionDao transactionDao) {
            this.transactionDao = transactionDao;
        }

        @Override
        protected Void doInBackground(Transaction... transactions) {
            transactionDao.insert(transactions[0]);
            return null;
        }
    }

    private static class DeleteTransactionTask extends AsyncTask<Transaction, Void, Void> {

        private TransactionDao transactionDao;

        private DeleteTransactionTask(TransactionDao transactionDao) {
            this.transactionDao = transactionDao;
        }

        @Override
        protected Void doInBackground(Transaction... items) {
            transactionDao.delete(items[0]);
            return null;
        }
    }

    private static class UpdateTransactionTask extends AsyncTask<Transaction, Void, Void> {

        private TransactionDao transactionDao;

        private UpdateTransactionTask(TransactionDao transactionDao) {
            this.transactionDao = transactionDao;
        }

        @Override
        protected Void doInBackground(Transaction... items) {
            transactionDao.update(items[0]);
            return null;
        }
    }

    private static class DeleteAllTask extends AsyncTask<Void, Void, Void> {

        private TransactionDao transactionDao;

        private DeleteAllTask(TransactionDao transactionDao) {
            this.transactionDao = transactionDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            transactionDao.deleteAllData();
            return null;
        }
    }
}
