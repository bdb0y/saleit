package dev.amin.saleit.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import dev.amin.saleit.database.Database;
import dev.amin.saleit.database.data_access_objects.ItemTransactionDao;
import dev.amin.saleit.models.ItemTransaction;

public class ItemTransactionRepository {
    private ItemTransactionDao dao;
    private LiveData<List<ItemTransaction>> itemTransactions;

    public ItemTransactionRepository(Application application) {
        Database database = Database.getInstance(application);
        dao = database.getItemTransactionDao();
        itemTransactions = dao.getItemTransactions();
    }

    public void insert(ItemTransaction customer) {
        new InsertTask(dao).execute(customer);
    }

    public void delete(ItemTransaction customer) {
        new DeleteTask(dao).execute(customer);
    }

    public void update(ItemTransaction customer) {
        new UpdateTask(dao).execute(customer);
    }

    public LiveData<List<ItemTransaction>> getCustomers() {
        return itemTransactions;
    }

    private static class InsertTask extends AsyncTask<ItemTransaction, Void, Void> {
        private ItemTransactionDao dao;

        private InsertTask(ItemTransactionDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(ItemTransaction... customer) {
            dao.insert(customer[0]);
            return null;
        }
    }

    private static class DeleteTask extends AsyncTask<ItemTransaction, Void, Void> {

        private ItemTransactionDao dao;

        private DeleteTask(ItemTransactionDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(ItemTransaction... customer) {
            dao.delete(customer[0]);
            return null;
        }
    }

    private static class UpdateTask extends AsyncTask<ItemTransaction, Void, Void> {

        private ItemTransactionDao dao;

        private UpdateTask(ItemTransactionDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(ItemTransaction... items) {
            dao.update(items[0]);
            return null;
        }
    }
}
