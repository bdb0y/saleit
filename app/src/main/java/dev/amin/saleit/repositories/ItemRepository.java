package dev.amin.saleit.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import dev.amin.saleit.database.Database;
import dev.amin.saleit.database.data_access_objects.ItemDao;
import dev.amin.saleit.models.Item;

public class ItemRepository {
    private ItemDao itemDao;
    private LiveData<List<Item>> items;

    public ItemRepository(Application application) {
        Database database = Database.getInstance(application);
        itemDao = database.getItemDao();
        items = itemDao.getItems();
    }

    public void insertItem(Item item) {
        new InsertItemTask(itemDao).execute(item);
    }

    public void deleteItem(Item item) {
        new DeleteItemTask(itemDao).execute(item);
    }

    public void updateItem(Item item) {
        new UpdateItemTask(itemDao).execute(item);
    }

    public LiveData<List<Item>> getItems(){
        return items;
    }

    private static class InsertItemTask extends AsyncTask<Item, Void, Void> {
        private ItemDao itemDao;

        private InsertItemTask(ItemDao itemDao) {
            this.itemDao = itemDao;
        }

        @Override
        protected Void doInBackground(Item... items) {
            itemDao.insert(items[0]);
            return null;
        }
    }

    private static class DeleteItemTask extends AsyncTask<Item, Void, Void> {

        private ItemDao itemDao;

        private DeleteItemTask(ItemDao itemDao) {
            this.itemDao = itemDao;
        }

        @Override
        protected Void doInBackground(Item... items) {
            itemDao.delete(items[0]);
            return null;
        }
    }

    private static class UpdateItemTask extends AsyncTask<Item, Void, Void> {

        private ItemDao itemDao;

        private UpdateItemTask(ItemDao itemDao) {
            this.itemDao = itemDao;
        }

        @Override
        protected Void doInBackground(Item... items) {
            itemDao.update(items[0]);
            return null;
        }
    }
}
