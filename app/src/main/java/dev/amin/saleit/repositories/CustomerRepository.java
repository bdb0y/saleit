package dev.amin.saleit.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import dev.amin.saleit.database.Database;
import dev.amin.saleit.database.data_access_objects.CustomerDao;
import dev.amin.saleit.models.Customer;

public class CustomerRepository {
    private CustomerDao dao;
    private LiveData<List<Customer>> customers;

    public CustomerRepository(Application application) {
        Database database = Database.getInstance(application);
        dao = database.getCustomerDao();
        customers = dao.getCustomers();
    }

    public void insert(Customer customer) {
        new InsertTask(dao).execute(customer);
    }

    public void delete(Customer customer) {
        new DeleteTask(dao).execute(customer);
    }

    public void update(Customer customer) {
        new UpdateTask(dao).execute(customer);
    }

    public LiveData<List<Customer>> getCustomers() {
        return customers;
    }

    private static class InsertTask extends AsyncTask<Customer, Void, Void> {
        private CustomerDao dao;

        private InsertTask(CustomerDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Customer... customer) {
            dao.insertCustomer(customer[0]);
            return null;
        }
    }

    private static class DeleteTask extends AsyncTask<Customer, Void, Void> {

        private CustomerDao dao;

        private DeleteTask(CustomerDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Customer... customer) {
            dao.deleteCustomer(customer[0]);
            return null;
        }
    }

    private static class UpdateTask extends AsyncTask<Customer, Void, Void> {

        private CustomerDao dao;

        private UpdateTask(CustomerDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Customer... items) {
            dao.updateCustomer(items[0]);
            return null;
        }
    }
}