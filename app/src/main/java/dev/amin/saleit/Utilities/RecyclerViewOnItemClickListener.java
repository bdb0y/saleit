package dev.amin.saleit.Utilities;

public interface RecyclerViewOnItemClickListener {

    void OnItemClick(int position);

    void OnIncrement(int position);

    void OnDecrement(int position);
}
