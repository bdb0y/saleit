package dev.amin.saleit.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

import dev.amin.saleit.R;
import dev.amin.saleit.models.Customer;
import dev.amin.saleit.models.Transaction;
import dev.amin.saleit.view_models.CustomerViewModel;
import dev.amin.saleit.view_models.TransactionViewModel;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        TransactionViewModel customerViewModel = ViewModelProviders.of(this)
                .get(TransactionViewModel.class);
//        LiveData<List<Customer>> customers = customerViewModel.getCustomers();

//        customerViewModel.insertCustomer(new Customer("احمد"));
//        customerViewModel.insertCustomer(new Customer("مرتضی"));
//        customerViewModel.insertCustomer(new Customer("رضا"));
//        customerViewModel.insertCustomer(new Customer("اکبر"));
//        customerViewModel.insertCustomer(new Customer("میسشبیتنبم"));

        TextView textView = findViewById(R.id.testDatabase);

        customerViewModel.getTransactions().observe(this, items -> {
            String get = "";
            for (Transaction customer : items) {
                get = textView.getText().toString() + "\n";
                get += customer.toString();
                textView.setText(get);
            }

        });
    }
}
