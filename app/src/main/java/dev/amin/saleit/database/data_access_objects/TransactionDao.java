package dev.amin.saleit.database.data_access_objects;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import dev.amin.saleit.models.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TransactionDao {

    @Insert
    void insert(Transaction transaction);

    @Update
    void update(Transaction transaction);

    @Delete
    void delete(Transaction transaction);

    @Query("select * from transaction_table")
    LiveData<List<Transaction>> getTransactions();

    @Query("Delete from transaction_table")
    void deleteAllData();
}
