package dev.amin.saleit.database.data_access_objects;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import dev.amin.saleit.models.Customer;

@Dao
public interface CustomerDao {

    @Insert
    long insertCustomer(Customer customer);

    @Update
    void updateCustomer(Customer customer);

    @Delete
    void deleteCustomer(Customer customer);

    @Query("select * from customer")
    LiveData<List<Customer>> getCustomers();
}
