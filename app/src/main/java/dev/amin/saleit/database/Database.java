package dev.amin.saleit.database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import dev.amin.saleit.database.data_access_objects.CustomerDao;
import dev.amin.saleit.database.data_access_objects.ItemDao;
import dev.amin.saleit.database.data_access_objects.ItemTransactionDao;
import dev.amin.saleit.database.data_access_objects.TransactionDao;
import dev.amin.saleit.models.Customer;
import dev.amin.saleit.models.Item;
import dev.amin.saleit.models.ItemTransaction;
import dev.amin.saleit.models.Transaction;

@androidx.room.Database(entities = {Item.class, Transaction.class,
        Customer.class, ItemTransaction.class}, version = 7, exportSchema = false)
public abstract class Database extends RoomDatabase {

    private static Database database;

    public abstract ItemDao getItemDao();

    public abstract TransactionDao getTransactionDao();

    public abstract CustomerDao getCustomerDao();

    public abstract ItemTransactionDao getItemTransactionDao();

    public static synchronized Database getInstance(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context.getApplicationContext(),
                    Database.class, "business_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }
}
