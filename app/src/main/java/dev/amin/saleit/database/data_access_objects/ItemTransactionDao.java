package dev.amin.saleit.database.data_access_objects;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import dev.amin.saleit.models.ItemTransaction;

@Dao
public interface ItemTransactionDao {

    @Insert
    void insert(ItemTransaction itemTransaction);

    @Update
    void update(ItemTransaction itemTransaction);

    @Delete
    void delete(ItemTransaction itemTransaction);

    @Query("select * from item_transaction")
    LiveData<List<ItemTransaction>> getItemTransactions();
}