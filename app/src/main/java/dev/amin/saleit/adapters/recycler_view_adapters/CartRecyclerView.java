package dev.amin.saleit.adapters.recycler_view_adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.amin.saleit.R;
import dev.amin.saleit.Utilities.RecyclerViewOnItemClickListener;
import dev.amin.saleit.models.CartItem;

public class CartRecyclerView extends RecyclerView.Adapter<CartRecyclerView.CartViewHolder> {

    private List<CartItem> cartItems;
    private RecyclerViewOnItemClickListener onItemClickListener;

    public void setOnThisItemClickListener(RecyclerViewOnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public CartRecyclerView(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CartViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.cart_item, parent, false), onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        CartItem cartItem = cartItems.get(position);
        holder.amount.setText(String.valueOf(cartItem.getAmount()));
        holder.name.setText(cartItem.getName());
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder {
        private TextView amount, name;

        public CartViewHolder(@NonNull View itemView, final RecyclerViewOnItemClickListener listener) {
            super(itemView);

            amount = itemView.findViewById(R.id.cartItemAmount);
            name = itemView.findViewById(R.id.cartItemName);
            ImageButton add = itemView.findViewById(R.id.cartItemAdd);
            ImageButton remove = itemView.findViewById(R.id.cartItemRemove);

            add.setOnClickListener(v -> {
                if (listener != null) {
                    int adapterPosition = getAdapterPosition();
                    if (adapterPosition != RecyclerView.NO_POSITION)
                        listener.OnIncrement(adapterPosition);
                }
            });

            remove.setOnClickListener(v -> {
                if (listener != null) {
                    int adapterPosition = getAdapterPosition();
                    if (adapterPosition != RecyclerView.NO_POSITION)
                        listener.OnDecrement(adapterPosition);
                }
            });
        }
    }

    public void setItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }
}
