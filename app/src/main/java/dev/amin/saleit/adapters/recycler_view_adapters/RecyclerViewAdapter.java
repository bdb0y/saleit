package dev.amin.saleit.adapters.recycler_view_adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import dev.amin.saleit.R;
import dev.amin.saleit.models.Transaction;

public class RecyclerViewAdapter extends RecyclerView.Adapter {

    private List<Transaction> transactions;

    public RecyclerViewAdapter(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TransactionViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        TransactionViewHolder transactionViewHolder =
                (TransactionViewHolder) holder;
        Transaction transaction = transactions.get(position);
        transactionViewHolder.customerName.setText(transaction.getCustomerName());

        String cleanString = String.valueOf(transaction.getPrice())
                .replaceAll("[,٫.]", "");

        double parsed = Double.parseDouble(cleanString);

        String formatted = NumberFormat.getInstance(Locale.US).format(parsed/10);
        transactionViewHolder.transactionPrice.setText(formatted);
        transactionViewHolder.transactionDate.setText(String.valueOf(transaction.getDate()));

    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    public class TransactionViewHolder extends RecyclerView.ViewHolder {
        private TextView customerName, transactionPrice, transactionDate;

        public TransactionViewHolder(@NonNull View itemView) {
            super(itemView);
            customerName = itemView.findViewById(R.id.customerName);
            transactionPrice = itemView.findViewById(R.id.transactionPrice);
            transactionDate = itemView.findViewById(R.id.transactionDate);
        }
    }
}
