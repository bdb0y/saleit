package dev.amin.saleit.adapters.recycler_view_adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import dev.amin.saleit.R;
import dev.amin.saleit.models.Item;

public class ItemsRecyclerView extends RecyclerView.Adapter<ItemsRecyclerView.ItemsViewHolder> {

    private List<Item> items = new ArrayList<>();

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsViewHolder holder, int position) {
        Item item = items.get(position);
        holder.itemName.setText(item.getName());
//        holder.itemPrice.setText(String.valueOf(item.getPrice()));
        String cleanString = String.valueOf(item.getPrice()).replaceAll("[,٫.]", "");

        String formatted = format(cleanString);
        holder.itemPrice.setText(formatted);

        if (item.isNationality())
            holder.itemFlag.setImageResource(R.drawable.ic_united_states);
        else holder.itemFlag.setImageResource(R.drawable.ic_iran);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class ItemsViewHolder extends RecyclerView.ViewHolder {
        private TextView itemName, itemPrice;
        private ImageView itemFlag;

        public ItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.itemName);
            itemPrice = itemView.findViewById(R.id.itemPrice);
            itemFlag = itemView.findViewById(R.id.itemNationality);
        }
    }

    public String format(String text) {
        List<Character> unformatted = new ArrayList<>();
        int counter = 0;
        for (int i = text.length() - 1; i >= 0; i--) {
            ++counter;
            unformatted.add(text.charAt(i));
            if (counter == 3 && i != 0) {
                unformatted.add(',');
                counter = 0;
            }
        }
        System.out.println(unformatted);
        String result = "";
        for (int i = unformatted.size() - 1; i >= 0; i--) {
            result += unformatted.get(i);
        }
        return result;
    }
}
