package dev.amin.saleit.adapters.recycler_view_adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.amin.saleit.R;
import dev.amin.saleit.models.Installment;

public class InstallmentRecyclerView extends
        RecyclerView.Adapter<InstallmentRecyclerView.InstallmentViewHolder> {

    private List<Installment> installments;

    public InstallmentRecyclerView(List<Installment> list) {
        this.installments = list;
    }

    @NonNull
    @Override
    public InstallmentRecyclerView.InstallmentViewHolder
    onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new InstallmentViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.installment_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull InstallmentRecyclerView.InstallmentViewHolder holder,
                                 int position) {
        Installment installment = installments.get(position);
        holder.customerName.setText(installment.getCustomerName());
        holder.price.setText(String.valueOf(installment.getPrice()));
        holder.count.setText(String.valueOf(installment.getCount()));
    }

    @Override
    public int getItemCount() {
        return installments.size();
    }

    public class InstallmentViewHolder extends RecyclerView.ViewHolder {
        private TextView customerName, price, count;

        public InstallmentViewHolder(@NonNull View itemView) {
            super(itemView);
            customerName = itemView.findViewById(R.id.installmentCustomerName);
            price = itemView.findViewById(R.id.installmentTransactionPrice);
            count = itemView.findViewById(R.id.installmentCount);
        }
    }
}